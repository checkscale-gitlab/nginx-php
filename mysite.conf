server {
    # Redirect all requests from http:// to https://
    
    listen 80 default_server;
    listen [::]:80 default_server;
    server_name eduardozuchetti.tk;
    return 301 https://$host$request_uri;
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2 ipv6only=on; ## listen for ipv6

    root /var/www/public;

    index index.php index.html;
    
    # Make acessible from localhost
    server_name _;
    
    # SSL Configurations
    # Replace ##DOMAIN## woth your domain
    ssl_certificate     /etc/letsencrypt/live/##DOMAIN##/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/##DOMAIN##/privkey.pem;
    ssl_session_cache shared:SSL:1m;
    ssl_session_timeout  10m;
    ssl_ciphers HIGH:!aNULL:!MD5;
    ssl_prefer_server_ciphers on;

    sendfile off;

    # This logs will be acessible from /data/nginx/logs/ out of container
    error_log  /var/log/nginx/error.log;
    access_log /var/log/nginx/access.log;

    # PHP configurations
    location ~ \.php$ {
        try_files $uri =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass php:9000;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
    }

    location ~* \.(jpg|jpeg|gif|png|css|js|ico|webp|tiff|ttf|svg)$ {
            expires           5d;
    }

    location ~ /\. {
            log_not_found off;
            deny all;
    }

    # For Let's Encrypt: This allow access to the yousite.com/.well-known
    # for create your free ssl certificate. If you will not use Let's Encrypt
    # you can remove this "location"
    
    location ^~ /.well-known {
            allow all;
            auth_basic off;
    }
}